package sample;

import javafx.beans.property.SimpleBooleanProperty;

public class Subject {

    private String subName;
    private String subID;
    private String credit;
    private String subBasic;
    private String level;
    private SimpleBooleanProperty isLeaned;

    public SimpleBooleanProperty getIsLeaned(){
        return isLeaned;
    }

    public Subject(String s,String i,String  c,String b,String l) {
        this.subName = s; this.subID = i;
        this.credit = c; this.subBasic = b;
        this.level = l;
        this.isLeaned = new SimpleBooleanProperty(false);
    }


    public String getSubName() {
        return subName;
    }

    public String getSubID() {
        return subID;
    }

    public String getCredit() {
        return credit;
    }

    public String getSubBasic() {
        return subBasic;
    }

    public String getLevel() {
        return level;
    }


//    public void setSubName(String subName) {
//        this.subName = subName;
//    }

    public void setSubID(String subID) {
        this.subID = subID;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public void setSubBasic(String subBasic) {
        this.subBasic = subBasic;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
